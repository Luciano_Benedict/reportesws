--------------------------------------------------------
-- Archivo creado  - jueves-febrero-08-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table TBL_DS_PROCESOS_ANALISIS
--------------------------------------------------------

  CREATE TABLE "SDAC"."TBL_DS_PROCESOS_ANALISIS" 
   (	"ID" NUMBER(*,0), 
	"ID_SOLICITUD_AMDOCS" NUMBER(*,0), 
	"DATA_STRING" VARCHAR2(3000 BYTE), 
	"FECHA_PROCESO" TIMESTAMP (6), 
	"ESTADO_ED" VARCHAR2(50 BYTE), 
	"ERROR" VARCHAR2(2000 BYTE), 
	"ID_SOLICITUD_IRIS" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table TBL_DS_PROCESOS_ANALISIS
--------------------------------------------------------

  ALTER TABLE "SDAC"."TBL_DS_PROCESOS_ANALISIS" MODIFY ("ID_SOLICITUD_AMDOCS" NOT NULL ENABLE);
  ALTER TABLE "SDAC"."TBL_DS_PROCESOS_ANALISIS" MODIFY ("ID" NOT NULL ENABLE);
