﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReportesWS
{
    public partial class frListaErrores : Form
    {

        public static string strListaErrores;

        ListaErrores _ListaErrores;

        public frListaErrores()
        {
            InitializeComponent();
            strListaErrores = "LE.xml";
        }

        void CargarLista(string _pDirectorio)
        {
            
            try {
                _ListaErrores.CargarCodigoErrires(System.IO.Path.GetDirectoryName(_pDirectorio) + "\\" + strListaErrores);

                for (int i = 0; i < _ListaErrores.CodigosErrores.Count; i++)
                {
                    lsErrores.Items.Add(_ListaErrores.CodigosErrores[i].cod.ToString());
                }
            }
            catch (Exception)
            {

            }
        }

        private void frListaErrores_Load(object sender, EventArgs e)
        {
            MaximizeBox = false;
            _ListaErrores = new ListaErrores();
            if(System.IO.File.Exists(System.IO.Directory.GetCurrentDirectory() + "\\" + strListaErrores))
            {
                CargarLista((System.IO.Directory.GetCurrentDirectory() + "\\" + strListaErrores));
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string valor = textBox1.Text;

            if (string.IsNullOrEmpty(valor))
            {
                MessageBox.Show("debe escribir un valor");
                textBox1.Focus();
                return;
            }

            try
            {
                int.Parse(valor);
            }
            catch (Exception)
            {
                MessageBox.Show("El valor debe ser numerico");
                return;
            }

            if (!lsErrores.Items.Contains(valor)) { 
                lsErrores.Items.Add(valor);
                CodError codigo= new CodError();
                codigo.cod = int.Parse(valor);
                _ListaErrores.CodigosErrores.Add(codigo);
            }
        }


        private void lsErrores_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void lsErrores_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Delete)
            {
                try
                {

                    _ListaErrores.CodigosErrores.RemoveAt(lsErrores.SelectedIndex);
                    lsErrores.Items.RemoveAt(lsErrores.SelectedIndex);
                }
                catch (Exception)
                {
                    MessageBox.Show("No hay items");
                }
            }
        }

        private void lsErrores_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(_ListaErrores.GuardarCodigoErrores(System.IO.Path.GetFileNameWithoutExtension(strListaErrores), System.IO.Directory.GetCurrentDirectory() + "\\"))
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Error");
            }
        }
    }
}
