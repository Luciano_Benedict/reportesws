﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace ReportesWS
{
    public partial class Calendario : Form
    {
        private string fecha;
        private DateTime dt;

        public Calendario()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }
        void MonthCalendar1DateChanged(object sender, DateRangeEventArgs e)
        {

        }
        void MonthCalendar1DateSelected(object sender, DateRangeEventArgs e)
        {
            fecha = calen.SelectionRange.Start.ToString("dd/MM/yyyy");
        }

        public string getFecha() { return fecha; }
        void BtAceptarClick(object sender, EventArgs e)
        {
            fecha = calen.SelectionRange.Start.ToString("dd/MM/yyyy");
            dt = new DateTime(int.Parse(fecha.Substring(6, 4)), int.Parse(fecha.Substring(3, 2)), int.Parse(fecha.Substring(0, 2)));
        }

        public string ObtenerFechaActual()
        {
            return calen.SelectionRange.Start.ToString("dd/MM/yyyy");
        }

        public int GetNroDia()
        {

            switch (dt.DayOfWeek.ToString())
            {
                case "Monday":
                    return 1;

                case "Tuesday":
                    return 2;
                case "Wednesday":
                    return 3;
                case "Thursday":
                    return 4;
                case "Friday":
                    return 5;
                case "Saturday":
                    return 6;
                case "Sunday":
                    return 7;

            }
            return (-1);
        }


        public string GetDia()
        {

            switch (dt.DayOfWeek.ToString())
            {
                case "Monday":
                    return "Lunes";
                case "Tuesday":
                    return "Martes";
                case "Wednesday":
                    return "Miercoles";
                case "Thursday":
                    return "Jueves";
                case "Friday":
                    return "Viernes";
                case "Saturday":
                    return "Sabado";
                case "Sunday":
                    return "Domingo";

            }
            return null;
        }


        public string GetDiaSemana()
        {


            return dt.DayOfWeek.ToString();
        }

        public int GetNroSemana()
        {

            return System.Globalization.CultureInfo.CurrentUICulture.Calendar.GetWeekOfYear(dt, CalendarWeekRule.FirstDay, dt.DayOfWeek);


        }

        public string ObtenerFechaConsulta()
        {
            string fecha2;

            fecha2 = fecha.Substring(6, 4);
            fecha2 += "-";
            fecha2 += fecha.Substring(3, 2);
            fecha2 += "-";
            fecha2 += fecha.Substring(0, 2);

            return fecha2;
        }


        void CalendarioLoad(object sender, EventArgs e)
        {

        }

        private void btAceptar_Click(object sender, EventArgs e)
        {
            fecha = calen.SelectionRange.Start.ToString("dd/MM/yyyy");
            DialogResult = DialogResult.OK;
        }
    }
}
