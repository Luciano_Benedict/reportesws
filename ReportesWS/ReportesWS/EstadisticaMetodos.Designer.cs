﻿namespace ReportesWS
{
    partial class EstadisticaMetodos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.lsMetodos = new System.Windows.Forms.ListBox();
            this.lsCodigos = new System.Windows.Forms.ListBox();
            this.lsContCodigos = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Grafico = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label4 = new System.Windows.Forms.Label();
            this.btExportar = new System.Windows.Forms.Button();
            this.lbLotes = new System.Windows.Forms.Label();
            this.lbReg = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Grafico)).BeginInit();
            this.SuspendLayout();
            // 
            // lsMetodos
            // 
            this.lsMetodos.FormattingEnabled = true;
            this.lsMetodos.Location = new System.Drawing.Point(12, 49);
            this.lsMetodos.Name = "lsMetodos";
            this.lsMetodos.Size = new System.Drawing.Size(171, 342);
            this.lsMetodos.TabIndex = 0;
            this.lsMetodos.SelectedIndexChanged += new System.EventHandler(this.lsMetodos_SelectedIndexChanged);
            // 
            // lsCodigos
            // 
            this.lsCodigos.FormattingEnabled = true;
            this.lsCodigos.Location = new System.Drawing.Point(189, 49);
            this.lsCodigos.Name = "lsCodigos";
            this.lsCodigos.Size = new System.Drawing.Size(171, 342);
            this.lsCodigos.TabIndex = 1;
            // 
            // lsContCodigos
            // 
            this.lsContCodigos.FormattingEnabled = true;
            this.lsContCodigos.Location = new System.Drawing.Point(366, 49);
            this.lsContCodigos.Name = "lsContCodigos";
            this.lsContCodigos.Size = new System.Drawing.Size(171, 342);
            this.lsContCodigos.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Metodos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(189, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Codigos";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(362, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Cantidad";
            // 
            // Grafico
            // 
            chartArea1.Name = "ChartArea1";
            this.Grafico.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.Grafico.Legends.Add(legend1);
            this.Grafico.Location = new System.Drawing.Point(561, 49);
            this.Grafico.Name = "Grafico";
            this.Grafico.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Grayscale;
            this.Grafico.Size = new System.Drawing.Size(556, 441);
            this.Grafico.TabIndex = 6;
            this.Grafico.Text = "Grafico";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(557, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(326, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Cantidad de Errores totales por Metodo";
            // 
            // btExportar
            // 
            this.btExportar.Location = new System.Drawing.Point(1022, 12);
            this.btExportar.Name = "btExportar";
            this.btExportar.Size = new System.Drawing.Size(95, 23);
            this.btExportar.TabIndex = 8;
            this.btExportar.Text = "Exportar";
            this.btExportar.UseVisualStyleBackColor = true;
            this.btExportar.Click += new System.EventHandler(this.btExportar_Click);
            // 
            // lbLotes
            // 
            this.lbLotes.AutoSize = true;
            this.lbLotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLotes.Location = new System.Drawing.Point(12, 409);
            this.lbLotes.Name = "lbLotes";
            this.lbLotes.Size = new System.Drawing.Size(54, 20);
            this.lbLotes.TabIndex = 9;
            this.lbLotes.Text = "Lotes";
            // 
            // lbReg
            // 
            this.lbReg.AutoSize = true;
            this.lbReg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbReg.Location = new System.Drawing.Point(12, 442);
            this.lbReg.Name = "lbReg";
            this.lbReg.Size = new System.Drawing.Size(158, 20);
            this.lbReg.TabIndex = 10;
            this.lbReg.Text = "Registros por Lote";
            // 
            // EstadisticaMetodos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1129, 508);
            this.Controls.Add(this.lbReg);
            this.Controls.Add(this.lbLotes);
            this.Controls.Add(this.btExportar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Grafico);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lsContCodigos);
            this.Controls.Add(this.lsCodigos);
            this.Controls.Add(this.lsMetodos);
            this.Name = "EstadisticaMetodos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estadistica Metodos";
            this.Load += new System.EventHandler(this.EstadisticaMetodos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Grafico)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lsMetodos;
        private System.Windows.Forms.ListBox lsCodigos;
        private System.Windows.Forms.ListBox lsContCodigos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataVisualization.Charting.Chart Grafico;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btExportar;
        private System.Windows.Forms.Label lbLotes;
        private System.Windows.Forms.Label lbReg;
    }
}