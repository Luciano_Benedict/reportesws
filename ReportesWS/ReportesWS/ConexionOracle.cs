﻿
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Web;
using Oracle.DataAccess.Client;
using Oracle;
using Oracle.DataAccess.Types;
using System;
public class ConexionOracle
{
    protected string rutaConexion;
    protected OracleConnection conector;
    protected OracleCommand comando;
    protected OracleDataAdapter adaptador;
    protected OracleParameter parametro;
    protected OracleDataReader rd;
    protected DataTable dtResultados;
    protected DataSet ds;
    protected stcEstado m_estado;
    public struct stcEstado
    {
        public string mensaje;
        public string observacion;
        public bool error_;
    }

    public ConexionOracle(string _r)
    {
        PrepararConexion(_r);
        InitEstado();
        ds = new DataSet();
    }


    public bool VerificarConexion()
    {

        InitEstado();
        try
        {
            Conectar();
            Desconectar();
        }catch(Exception ex)
        {

            m_estado.error_ = true;
            m_estado.mensaje = ex.Message;
            m_estado.observacion = "Error al conectar";

            return false;
        }

        return true;
    }


    protected void InitEstado()
    {
        m_estado = new stcEstado();
        m_estado.mensaje = "OK";
        m_estado.observacion = "";
        m_estado.error_ = false;
    }

    public stcEstado Estado
    {
        get
        {
            return m_estado;
        }
    }

    public void Conectar()
    {
        conector.Open();
    }

    public void Desconectar()
    {
        conector.Close();
    }

    public OracleConnection ObtenerConexion()
    {
        return conector;
    }

    public void LiberarConector()
    {
        conector.Dispose();
    }

    public void PrepararConexion(string _r)
    {
        rutaConexion = _r;
        conector = new OracleConnection(rutaConexion);
    }

    public DataTable DATATABLE
    {
        get
        {
            return dtResultados;
        }

        set
        {
            dtResultados = value;
        }
    }

    public void BuscarResultados(string comando)
    {
        try
        {
            InitEstado();
            Conectar();
            InstanciarTabla();
            InstanciarComando();
            adaptador = new OracleDataAdapter(comando, conector);
            adaptador.Fill(dtResultados);
            LiberarComando();
            Desconectar();
        }
        catch (System.Exception ex)
        {
            m_estado.mensaje = ex.Message;
            m_estado.observacion = Convert.ToString("Error al ejecutar consulta ") + comando;
            m_estado.error_ = true;
            Desconectar();
        }
    }

    public void BuscarResultados()
    {
        try
        {
            InitEstado();
            Conectar();
            InstanciarTabla();
            comando.Prepare();
            adaptador = new OracleDataAdapter(comando);
            adaptador.Fill(dtResultados);
            LiberarComando();
            Desconectar();
        }
        catch (Exception ex)
        {
            m_estado.mensaje = ex.Message;
            m_estado.observacion = "Error al ejecutar consulta " + comando.CommandText;
            m_estado.error_ = true;
            Desconectar();
        }
    }

    public void BuscarResultadosConParametros(string ConsultaParametros)
    {
        try
        {
            InitEstado();
            Conectar();
            InstanciarTabla();
            comando.CommandText = ConsultaParametros;
            comando.CommandType = CommandType.Text;
            adaptador = new OracleDataAdapter(comando);
            adaptador.Fill(dtResultados);
            LiberarComando();
            Desconectar();
        }
        catch (Exception ex)
        {
            m_estado.mensaje = ex.Message;
            m_estado.observacion = "Error al ejecutar consulta " + comando.CommandText;
            m_estado.error_ = true;
            LiberarParametros();
            Desconectar();
        }
    }

    public bool HayResultados()
    {
        try
        {
            if ((dtResultados.Rows.Count > 0))
            {
                return true;
            }
        }
        catch (Exception)
        {

        }
        return false;
    }

    private void InstanciarTabla(String tabla)
    {
        dtResultados = new DataTable(tabla);
    }

    private void InstanciarTabla(DataTable _dt)
    {
        dtResultados = _dt;
    }

    private void InstanciarTabla()
    {
        dtResultados = new DataTable();
    }

    public DataTable ObtenerResultados()
    {
        return dtResultados;
    }

    public DataTable ObtenerResultados(string tabla)
    {
        return ds.Tables[tabla];
    }

    public DataTable ObtenerAgregados()
    {
        return dtResultados.GetChanges(DataRowState.Added);
    }

    public DataTable ObtenerModificados()
    {
        return dtResultados.GetChanges(DataRowState.Modified);
    }

    public void InstanciarAdaptador(string consultaSql)
    {
        adaptador = new OracleDataAdapter(consultaSql, conector);
    }

    public void LiberarAdaptador()
    {
        adaptador = null;
    }

    public DataSet DATASET
    {
        get
        {
            return ds;
        }

        set
        {
            ds = value;
        }
    }

    public void InstanciarDS()
    {
        ds = new DataSet();
    }

    public void BuscarResultadosDS(string tabla)
    {
        ds = new DataSet();
        Conectar();
        adaptador = new OracleDataAdapter(comando.CommandText, conector);
        adaptador.Fill(ds, tabla);
        LiberarComando();
        comando.Parameters.Clear();
        Desconectar();
    }

    public DataSet ObtenerResultadosDS()
    {
        return ds;
    }

    public void LimpiarDS()
    {
        ds.Tables.Clear();
    }

    public void LiberarDS()
    {
        ds.Dispose();
    }

    public OracleDataReader Reader
    {
        get
        {
            return rd;
        }
    }

    public void PrepararReader()
    {
        Conectar();
        rd = comando.ExecuteReader();
    }

    public void PrepararReader(string consulta)
    {
        Conectar();
        comando.CommandText = consulta;
        rd = comando.ExecuteReader();
    }

    public void LiberarReader()
    {
        Desconectar();
        rd.Close();
        rd.Dispose();
    }

    public void InstanciarComando()
    {
        comando = new OracleCommand();
        comando.Connection = conector;
    }

    public void InstanciarComando(string c)
    {
        comando = new OracleCommand();
        comando.Connection = conector;
        comando.CommandText = c;
    }

    public void setComando(OracleCommand c)
    {
        comando = c;
    }

    public OracleCommand ObtenerComando()
    {
        return comando;
    }

    public void LiberarComando()
    {
        comando.Dispose();
    }

    public void InstanciarComandoProcAlmacenado(string strConsulta)
    {
        comando = new OracleCommand();
        comando.Connection = ObtenerConexion();
        comando.CommandText = strConsulta;
        comando.CommandType = CommandType.StoredProcedure;
    }

    public void InstanciarComandoProcAlmacenado()
    {
        comando = new OracleCommand();
        comando.Connection = ObtenerConexion();
        comando.CommandType = CommandType.StoredProcedure;
    }

    public void EjecutarConsultaComando()
    {
        comando.ExecuteNonQuery();
    }

    public void InstanciarParametro()
    {
        parametro = new OracleParameter();
    }

    public void AgregarParametro(string NombreParametro, OracleDbType Tipo, string valor)
    {
        if (comando == null)
        {
            InstanciarComando();
        }

        parametro = new OracleParameter(NombreParametro, Tipo, ParameterDirection.Input);
        parametro.Value = valor;
        comando.Parameters.Add(parametro);
    }

    public void AgregarParametro(string NombreParametro, OracleDbType Tipo, Int64 valor)
    {
        if (comando == null)
        {
            InstanciarComando();
        }

        parametro = new OracleParameter(NombreParametro, Tipo, ParameterDirection.Input);
        parametro.Value = valor;
        comando.Parameters.Add(parametro);
    }

    public void AgregarParametroAsociandoValor(string NombreParametro, object valor)
    {
        if (comando == null)
        {
            InstanciarComando();
            comando.BindByName = true;
        }

        comando.Parameters.Add(NombreParametro, valor);
    }

    public void AgregarParametro(string NombreParametro, OracleDbType Tipo, int tam, string valor)
    {
        if (comando == null)
        {
            InstanciarComando();
        }

        parametro = new OracleParameter(NombreParametro, Tipo, tam);
        parametro.Value = valor;
        comando.Parameters.Add(parametro);
    }

    public void AgregarParametroProcAlmacenado(string NombreParametro, OracleDbType Tipo, string valor)
    {
        if (comando == null)
        {
            InstanciarComandoProcAlmacenado();
        }

        parametro = new OracleParameter(NombreParametro, Tipo);
        parametro.Value = valor;
        comando.Parameters.Add(parametro);
    }

    public void AgregarParametroProcAlmacenado(string NombreParametro, OracleDbType Tipo, int tam, string valor)
    {
        if (comando == null)
        {
            InstanciarComandoProcAlmacenado();
        }

        parametro = new OracleParameter(NombreParametro, Tipo, tam);
        parametro.Value = valor;
        comando.Parameters.Add(parametro);
    }

    public void AplicarParametros()
    {
        comando.Parameters.Add(parametro);
    }

    public void LiberarParametros()
    {
        parametro = null;
    }

    public bool EjecutarSP(string sp)
    {
        InitEstado();
        Conectar();
        try
        {
            if (comando == null)
            {
                InstanciarComandoProcAlmacenado(sp);
            }
            else
            {
                comando.CommandText = sp;
            }

            EjecutarConsultaComando();
            LiberarComando();
        }
        catch (Exception ex)
        {
            Desconectar();
            LiberarComando();
            m_estado.mensaje = ex.Message;
            m_estado.observacion = "Error al ejecutar procedimiento Almacenado";
            m_estado.error_ = true;
            return false;
        }

        Desconectar();
        return true;
    }

    public bool EjecutarSP(OracleCommand c)
    {
        InitEstado();
        Conectar();
        try
        {
            c.ExecuteNonQuery();
            c.Dispose();
        }
        catch (Exception ex)
        {
            m_estado.mensaje = ex.Message;
            m_estado.observacion = "Error al ejecutar procedimiento Almacenado";
            m_estado.error_ = true;
            Desconectar();
            return false;
        }

        Desconectar();
        return true;
    }

    public bool EjecutarSP()
    {
        InitEstado();
        Conectar();
        try
        {
            comando.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            m_estado.mensaje = ex.Message;
            m_estado.observacion = "Error al ejecutar procedimiento Almacenado";
            m_estado.error_ = true;
            Desconectar();
            return false;
        }

        Desconectar();
        return true;
    }

    public bool EjecutarConsulta(string consulta)
    {
        InitEstado();
        Conectar();
        InstanciarComando(consulta);
        try
        {
            comando.ExecuteNonQuery();
        }
        catch (Exception generatedExceptionName)
        {
            Desconectar();
            LiberarComando();
            m_estado.mensaje = generatedExceptionName.Message;
            m_estado.observacion = "Error al consultar";
            m_estado.error_ = true;
            return false;
        }

        Desconectar();
        LiberarComando();
        return true;
    }

    public bool EjecutarConsultaConParametros(string consulta)
    {
        InitEstado();
        Conectar();
        comando.CommandText = consulta;
        try
        {
            comando.ExecuteNonQuery();
        }
        catch (Exception generatedExceptionName)
        {
            Desconectar();
            comando.Parameters.Clear();
            LiberarComando();
            m_estado.mensaje = generatedExceptionName.Message;
            m_estado.observacion = "Error al consultar";
            m_estado.error_ = true;
            return false;
        }

        Desconectar();
        comando.Parameters.Clear();
        LiberarComando();
        return true;
    }

    public bool EjecutarConsulta()
    {
        InitEstado();
        Conectar();
        try
        {
            comando.ExecuteNonQuery();
        }
        catch (Exception generatedExceptionName)
        {
            Desconectar();
            LiberarComando();
            m_estado.mensaje = generatedExceptionName.Message;
            m_estado.observacion = "Error al consultar";
            m_estado.error_ = true;
            return false;
        }

        Desconectar();
        LiberarComando();
        return true;
    }
}

