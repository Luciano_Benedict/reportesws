﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



public partial class CargaDBEstandar : Form
{

    private bool _MuestraIteracion;


    public bool MuestraIteracion
    {
        set
        {
            _MuestraIteracion = MuestraIteracion = timer1.Enabled = label1.Visible = value;
        }
        get
        {
            return _MuestraIteracion;
        }
    }

    public CargaDBEstandar()
    {
        _MuestraIteracion = false;
        InitializeComponent();   
    }

    private void CargaDBEstandar_Load(object sender, EventArgs e)
    {

    }
        
    public void Mostrar()
    {
        CargaDBEstandar CDBE = this;
        CDBE.ShowDialog();

    }

    private void timer1_Tick(object sender, EventArgs e)
    {
        if(MuestraIteracion)lbIteracion.Text = Recurso.Iterador + " de " + Recurso.MAX;
    }
}

