﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReportesWS
{
    public partial class Estadistica : Form
    {
        DataTable dtEstadistica;
        List<int> CodigoErrores;
        List<int> ContCodigoErrores;
        int totalTramitesErrores;
        int ObtenerTotal(List<int> _pContErrores)
        {
            int total = 0;
            for(int i = 0; i < _pContErrores.Count; i++)
            {
                total += _pContErrores[i];
            }
            return total;
        }


        void Exportar()
        {
            string strArchivo = "Estadistica_"+DateTime.Now+".txt";
        }

        public Estadistica(List<int> _pCodigoErrores, List<int> _pContCodigoErrores, int _pCantLotes, int _pCantLotesMAx , int _pNroRegXLote)
        {

            InitializeComponent();
            CodigoErrores = _pCodigoErrores;
            ContCodigoErrores = _pContCodigoErrores;
            dtEstadistica = new DataTable();
            totalTramitesErrores = ObtenerTotal(_pContCodigoErrores);
            lbEstadistica.Text = "Tramites Totales : " + Principal.LIMITE_REGISTROS_DB + " - Cant lotes a evaluar: " + _pCantLotes + "/" + _pCantLotesMAx + " - Tramites con errores: " + totalTramitesErrores;
            dtEstadistica.Columns.Add("Codigo");
            dtEstadistica.Columns.Add("Cantidad",(typeof(int)));
            dtEstadistica.Columns.Add("%");

            double total = 0;
            double total2 = 0;

            for (int i = 0; i < _pCodigoErrores.Count; i++)
            {
                DataRow fila = dtEstadistica.NewRow();
                fila["Codigo"] = _pCodigoErrores[i];
                fila["Cantidad"] = _pContCodigoErrores[i];
                total = ((_pContCodigoErrores[i] * 100.0) / totalTramitesErrores);
                fila["%"] = total.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
                total2 += total;
                dtEstadistica.Rows.Add(fila);
            }

            lbPorcentaje.Text = total2.ToString();
            grilla.DataSource = dtEstadistica;

            grilla.Sort(grilla.Columns["Cantidad"], ListSortDirection.Descending);

            if (CodigoErrores.Count > 0) { 
                ArmarGrafico();
            }

             
        }

        public void ArmarGrafico()
        {
            for(int i = 0; i < grilla.Rows.Count; i++)
            {   
                Grafico.Series.Add(grilla.Rows[i].Cells["Codigo"].Value.ToString() + " Cant: " + grilla.Rows[i].Cells["Cantidad"].Value.ToString());
                Grafico.Series[i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.RangeColumn;
                Grafico.Series[i].Points.Add(double.Parse(grilla.Rows[i].Cells["Cantidad"].Value.ToString()));
                Random R = new Random(i);
                Grafico.Series[i].Color = Color.FromArgb(R.Next(0, 255), R.Next(0, 255), R.Next(0, 255));
            }
        }

        private void Estadistica_Load(object sender, EventArgs e)
        {
            MaximizeBox = false;
        }

        private void Grafico_Click(object sender, EventArgs e)
        {

        }

        void ExportarForm(string _pDirectorio)
        {
            try { 
                var frm = Form.ActiveForm;
                using (var bmp = new Bitmap(frm.Width, frm.Height))
                {
                    frm.DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));
                    bmp.Save(_pDirectorio + "\\ESTADISTICA.bmp");
                }
            }catch(Exception ex)
            {
                MessageBox.Show("Error al generar imagen de estadistica " + ex.Message);
            }

        }

        void ExportarTxt(string _pDirectorio)
        {
            InOut Escritor = new InOut(_pDirectorio + "\\Estadistica.txt");
            List<string> listaEstadistica = new List<string>();
            listaEstadistica.Add("Estadistica " + DateTime.Now);
            listaEstadistica.Add("Tramites totales: " + Principal.LIMITE_REGISTROS_DB);
            listaEstadistica.Add("Tramites con errores: " + totalTramitesErrores);
            listaEstadistica.Add("-------------------------------");
            listaEstadistica.Add("CODIGO -->CANTIDAD-->PORCENTAJE");
            listaEstadistica.Add("-------------------------------");

            for (int i = 0; i < grilla.Rows.Count; i++)
            {
                listaEstadistica.Add(grilla.Rows[i].Cells[0].Value.ToString() +"-->" + grilla.Rows[i].Cells[1].Value.ToString() + "-->"+ grilla.Rows[i].Cells[2].Value.ToString());
            }

            Escritor.Escribir(listaEstadistica);

            if (Escritor.Estado.error_)
            {
                MessageBox.Show("Error al escribir estadistica en TXT. " + Environment.NewLine + Escritor.Estado.mensaje);
            }

        }

        public void Export()
        {
            string directorio = null;
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                directorio = folderBrowserDialog1.SelectedPath;
                ExportarForm(directorio);
                ExportarTxt(directorio);
                MessageBox.Show("Realizado");
            }

            folderBrowserDialog1.Dispose();
        }

        private void btExportar_Click(object sender, EventArgs e)
        {
            Export();
        }
    }
}
