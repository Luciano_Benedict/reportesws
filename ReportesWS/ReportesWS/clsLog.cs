﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public static class clsLog
{
    static InOut Escritor;
    public static InOut.stcEstado Estado;
    public static List<string> ListaErrores;

    public static void Agregar(string mensaje)
    {

        if(ListaErrores == null)
        {
            ListaErrores = new List<string>();
        }

    
        string msgLog = "[" + DateTime.Now + "] -> " + mensaje;

        ListaErrores.Add(msgLog);

    }

    public static void  GuardarListaErrores()
    {
        Escritor = new InOut(System.IO.Directory.GetCurrentDirectory() + "\\log.txt");
        Escritor.Escribir(ListaErrores);
        Estado = Escritor.Estado;
    }

    
}

