﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public static class Recurso
{
    /*
        * Esta clase se utiliza como recurso compartido entre el hilo de carga
        * y el proceso Padre. El proceso Padre incrementa el iterador, y el hilo de carga lo utiliza 
        * para mostrarlo en un label u otro objeto.
        * */
    public static int Iterador;
    public static int MAX;
    public static int Iterador2;
        
    public static void Iniciar()
    {
        Iterador = MAX = Iterador2 = 0;
    }

}

