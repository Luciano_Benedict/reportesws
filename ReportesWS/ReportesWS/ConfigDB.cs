﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReportesWS
{
    public partial class ConfigDB : Form
    {


        public ConfigDB()
        {
            InitializeComponent();
        }

        private void ConfigDB_Load(object sender, EventArgs e)
        {
            MaximizeBox = false;

            SysConfig sysConfig = new SysConfig();

            sysConfig.CargarDatos();
            txtHOST.Text = sysConfig.HOST;
            txtPASS.Text = sysConfig.PASSWORD;
            txtPORT.Text = sysConfig.PORT;
            txtSERVICE.Text = sysConfig.SERVICE;
            txtUSER.Text = sysConfig.USER;

        }

        public string ObtenerCadenaConexion()
        {
            string cadena =
                "Data Source=(DESCRIPTION="
                    + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + txtHOST.Text + ")(PORT=" + txtPORT.Text+ ")))"
                    + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + txtSERVICE.Text + ")));"
                    + "User Id=" + txtUSER.Text + ";Password=" + txtPASS.Text+ ";";

            return cadena;
        }

        private void btAceptar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtHOST.Text))
            {
                MessageBox.Show("Escriba un host");
                txtHOST.Focus();
                return;

            }

            if (string.IsNullOrEmpty(txtPORT.Text))
            {
                MessageBox.Show("Escriba un Puerto");
                txtPORT.Focus();
                return;

            }

            if (string.IsNullOrEmpty(txtSERVICE.Text))
            {
                MessageBox.Show("Escriba un servicio");
                txtSERVICE.Focus();
                return;

            }

            SysConfig _SysConfig = new SysConfig();

            _SysConfig.HOST = txtHOST.Text;
            _SysConfig.PORT = txtPORT.Text;
            _SysConfig.PASSWORD = txtPASS.Text;
            _SysConfig.USER = txtUSER.Text;
            _SysConfig.SERVICE = txtSERVICE.Text;

            if (!_SysConfig.Guardar())
            {
                MessageBox.Show("Error");
            }
            else
            {
                MessageBox.Show("Realizado");
                DialogResult = DialogResult.OK;
            }


        }
    }
}
