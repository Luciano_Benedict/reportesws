﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ReportesWS
{
    public struct CodError
    {
        public int cod;
    }
    public class ListaErrores
    {
        public List<CodError> CodigosErrores;



        public ListaErrores() {

            CodigosErrores = new List<CodError>();

        }


        public void CargarCodigoErrires(string _pDirectorio)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ListaErrores));
            FileStream fs = new FileStream(_pDirectorio, FileMode.Open);
            ListaErrores _Sys = (ListaErrores)serializer.Deserialize(fs);
            fs.Close();
            this.CodigosErrores = _Sys.CodigosErrores;
        }

        public bool GuardarCodigoErrores(string _pNombre, string _pDirectorio)
        {
            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(ListaErrores));
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(_pDirectorio + _pNombre + ".xml");
                writer.Serialize(file, this);
                file.Close();

            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

    }
}
