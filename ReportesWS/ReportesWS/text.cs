﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReportesWS
{
    public partial class txtCantidadMax : Form
    {

        public int Valor;
        int max;
        public txtCantidadMax(int _max)
        {
            InitializeComponent();
            max = _max;
            
        }

        private void text_Load(object sender, EventArgs e)
        {
            MaximizeBox = false;
            label2.Text = "Maximo "+max.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Valor = max;
            try {
                Valor = int.Parse(textBox1.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("el valor es incorrecto!");
                textBox1.Focus();
                return;
            }

            if(Valor > max)
            {
                MessageBox.Show("El valor no puede ser mayor al maximo!");
                textBox1.Focus();
                return;
            }

            if (Valor <= 0)
            {
                MessageBox.Show("El valor debe ser mayor a cero!");
                textBox1.Focus();
                return;
            }

            DialogResult = DialogResult.OK;

        }
    }
}
