﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.Serialization;
using System.IO;

public  class SysConfig
{
    public string HOST;
    public string SERVICE;
    public string PORT;
    public string USER;
    public string PASSWORD;
    public  bool Guardar()
    {
        System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(SysConfig));
        
        try
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(System.IO.Directory.GetCurrentDirectory() + "\\Config.xml");
            writer.Serialize(file, this);
            file.Close();
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    public string ObtenerCadenaConexion()
    {

        if(!File.Exists(System.IO.Directory.GetCurrentDirectory() + "\\Config.xml"))
        {
            return null;
        }

        SysConfig _SysConfig = new SysConfig();
        _SysConfig = Cargar();

        string cadena=
        "Data Source=(DESCRIPTION="
            + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST="+ _SysConfig.HOST + ")(PORT="+ _SysConfig.PORT + ")))"
            + "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME="+SERVICE+")));"
            + "User Id="+ _SysConfig.USER + ";Password="+ _SysConfig.PASSWORD + ";";

        return cadena;

    }


    public void CargarDatos()
    {

        if(File.Exists(System.IO.Directory.GetCurrentDirectory() + "\\Config.xml")) { 
            XmlSerializer serializer = new XmlSerializer(typeof(SysConfig));
            FileStream fs = new FileStream(System.IO.Directory.GetCurrentDirectory() + "\\Config.xml", FileMode.Open);
            SysConfig _Sys = (SysConfig)serializer.Deserialize(fs);
            fs.Close();
            this.HOST = _Sys.HOST;
            this.PORT = _Sys.PORT;
            this.SERVICE = _Sys.SERVICE;
            this.USER= _Sys.USER;
            this.PASSWORD = _Sys.PASSWORD;
        }
    }


    private SysConfig Cargar()
    {
         XmlSerializer serializer = new XmlSerializer(typeof(SysConfig));
        FileStream fs = new FileStream(System.IO.Directory.GetCurrentDirectory() + "\\Config.xml", FileMode.Open);
        SysConfig _Sys= (SysConfig)serializer.Deserialize(fs);
        fs.Close();
        return _Sys;
    }

}

