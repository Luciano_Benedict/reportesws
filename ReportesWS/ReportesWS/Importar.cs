﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReportesWS
{
    public partial class Importar : Form
    {

        string RutaArchivo;
        public static int ErroresEncontrados;

        Thread hilo;
        public List<string> RegistrosErroneos
        {
            get
            {
                return clsLog.ListaErrores;
            }
        }

        public Importar(string _pRutaArchivo)
        {
            InitializeComponent();
            RutaArchivo = _pRutaArchivo;
            ErroresEncontrados = 0;
            CargaBd.MAX = ObtenerCantidadRegistros();
            int MAX_USUARIO = -1;

            if(CargaBd.MAX == 0)
            {
                MessageBox.Show("El Archivo " + System.IO.Path.GetFileName(_pRutaArchivo) + " no corresponde. 0 registros a Importar");
                return;
            }

            DialogResult r = MessageBox.Show("El total a importar son " + CargaBd.MAX + " registros" + Environment.NewLine + "Desea editar esta cantidad?", "Editar?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (r == DialogResult.Yes)
            {

                ReportesWS.txtCantidadMax text = new ReportesWS.txtCantidadMax(CargaBd.MAX);
                if (text.ShowDialog() == DialogResult.OK)
                {
                    MAX_USUARIO = text.Valor;
                }
            }

            if (MAX_USUARIO > -1)
            {
                CargaBd.MAX = MAX_USUARIO;
            }
            
            LeerArchivoEImportar();
            DialogResult = DialogResult.OK;
        }

        private void btFecha_Click(object sender, EventArgs e)
        {
        
        }

        private void Importar_Load(object sender, EventArgs e)
        {
            
            MaximizeBox = false;
     

        }

        void procImportar(System.Data.DataTable _pTabla)
        {

            ConexionOracle Conexion = new ConexionOracle(Principal.cadenaConexion);

            for (int i = 0; i < _pTabla.Rows.Count; i++)
            {


                string cadena = "insert into TBL_DS_PROCESOS_ANALISIS( ID,ID_SOLICITUD_AMDOCS, DATA_STRING, FECHA_PROCESO, ESTADO_ED, IP,VERSION_PDF, OBSERVACIONES, LEYENDAS, EMAIL, ERROR " +
                            ", ID_SOLICITUD_IRIS) values ( " + _pTabla.Rows[i]["ID"].ToString() + "," + _pTabla.Rows[i]["ID_SOLICITUD_AMDOCS"].ToString() +
                            ",'" + _pTabla.Rows[i]["DATA_STRING"].ToString() + "',TO_DATE('" + _pTabla.Rows[i]["FECHA_PROCESO"].ToString() + "','dd/mm/yyyy HH24:MI:SS'),'" + _pTabla.Rows[i]["ESTADO_ED"].ToString() + "','" +
                            _pTabla.Rows[i]["IP"].ToString() + "','" + _pTabla.Rows[i]["VERSION_PDF"].ToString() + "','" + _pTabla.Rows[i]["OBSERVACIONES"].ToString() + "','" + _pTabla.Rows[i]["LEYENDAS"].ToString()
                            + "','" + _pTabla.Rows[i]["EMAIL"].ToString() + "','" + _pTabla.Rows[i]["ERROR"].ToString() + "'," + _pTabla.Rows[i]["ID_SOLICITUD_IRIS"].ToString() + ")";
                Conexion.EjecutarConsulta(cadena);

                if (Conexion.Estado.error_)
                {

                    break;

                }

            }

            MessageBox.Show(Conexion.Estado.mensaje + Environment.NewLine + Conexion.Estado.observacion);


        }

        int ObtenerCantidadRegistros()
        {
            int c = 0;
            StreamReader srLector = new StreamReader(RutaArchivo);
            while (!string.IsNullOrEmpty(srLector.ReadLine()))
            {

                c++;
            }

            srLector.Close();
            return c-1;
        }

        public static int ObtenerCantidadRegistrosDB()
        {
            ConexionOracle Conexion = new ConexionOracle(Principal.cadenaConexion);
            Conexion.BuscarResultados("select count(ID) from TBL_DS_PROCESOS_ANALISIS");
            if (Conexion.HayResultados())
            {
                if (!string.IsNullOrEmpty(Conexion.ObtenerResultados().Rows[0][0].ToString()))
                {
                    return int.Parse(Conexion.ObtenerResultados().Rows[0][0].ToString());
                }

            }

            if (Conexion.Estado.error_)
            {
                MessageBox.Show(Conexion.Estado.mensaje);
                return (-1);
            }

            return 0;
        }

        List<string> ObtenerCabeceraDB()
        {
            List<string> lsCabecera = new List<string>();

            ConexionOracle Conexion = new ConexionOracle(Principal.cadenaConexion);
            Conexion.BuscarResultados("select * from TBL_DS_PROCESOS_ANALISIS where rownum <=1");

            if (!Conexion.Estado.error_)
            {
                DataTable dt = Conexion.ObtenerResultados();
                for(int i = 0; i < dt.Columns.Count; i++)
                {
                    lsCabecera.Add(dt.Columns[i].Caption);
                }
            }
            else
            {
                MessageBox.Show(Conexion.Estado.mensaje);
            }

            return lsCabecera;
        }


        void LeerArchivoEImportar()
        {

            int Inicio = ObtenerCantidadRegistrosDB();

            if(Inicio == (-1))
            {
                return;
            }
            if (Inicio > 0) { 
                DialogResult r = MessageBox.Show("Hay "+Inicio+" registros actualmente en DB ¿Desea continuar la importacion a partir de esta cantidad?", "confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (r == DialogResult.No)
                {
                    Inicio = 0;
                }
            }

            CargaBd cc = new CargaBd();
            cc.MuestraErrores = true;
            hilo = new Thread(cc.Mostrar);
            hilo.Start();
            CargaBd.Iterador = 0;
            CargaBd.Mensaje = "Importando..";


            clsLog.ListaErrores = new List<string>();
            InOut L = new InOut();

            List<string> cabecera;
            if (L.Estado.error_)
            {
                MessageBox.Show("Error " + L.Estado.mensaje);
                return;
            }

            StreamReader srLector = new StreamReader(RutaArchivo);
            
            {
                cabecera = new List<string>();

                bool posicionarPunteroLectura = true;

                if (Inicio == 0) {
                    Inicio = 1;
                    posicionarPunteroLectura = false;
                }
                string _strCadena = srLector.ReadLine();
                _strCadena = _strCadena.Replace("\t", string.Empty);
                _strCadena = _strCadena.Replace("\\", string.Empty);
                _strCadena = _strCadena.Trim();
                string[] cabeceraAux = _strCadena.Split('"');
                for (int i = 1; i < cabeceraAux.Length; i += 2)
                {
                    cabecera.Add(cabeceraAux[i].Replace('"', ' '));
                }
                

                //ID, ID_SOLICITUD_AMDOCS, ID_SOLICITUD_IRIS, FECHA_PROCESO, DATA_STRING, ESTADO_ED, ERROR
                ConexionOracle ConexionDB = new ConexionOracle(Principal.cadenaConexion);
                
                for (int i = Inicio; i < CargaBd.MAX; i++)
                {
                    CargaBd.Iterador = i;

                    if (CargaBd.CancelarCarga)
                    {
                        break;
                    }

                    if (posicionarPunteroLectura)
                    {
                        int contPuntero = 1;
                        while (contPuntero < Inicio)
                        {
                            srLector.ReadLine();
                            contPuntero++;
                        }
                        posicionarPunteroLectura = false;
                    }

                    string contenidoFila = srLector.ReadLine();
                    try
                    {       
                        string[] Fila = contenidoFila.Split('\t');
                        string strConsulta = "Insert into TBL_DS_PROCESOS_ANALISIS (";

                        for (int f = 0; f < cabecera.Count; f++)
                        {
                            if (f == 0)
                            {
                                strConsulta = strConsulta + cabecera[f];
                            }
                            else
                            {
                                strConsulta = strConsulta + "," + cabecera[f];
                            }

                        }

                        strConsulta = strConsulta + ") values( ";

                        for (int j = 0; j < cabecera.Count; j++)
                        {

                            if (j == 0)
                            {
                                strConsulta = strConsulta + ":" + cabecera[j];
                            }
                            else
                            {
                                strConsulta = strConsulta + "," + ":" + cabecera[j];
                            }

                            Fila[j] = Fila[j].Replace("\\", "");
                            Fila[j] = Fila[j].Replace("\"", "");

                            if (cabecera[j].Contains("FECHA") && !string.IsNullOrEmpty(Fila[j]))
                            {
                                IFormatProvider culture = new CultureInfo("en-US", true);
                                ConexionDB.AgregarParametroAsociandoValor(":" + cabecera[j], DateTime.ParseExact(Fila[j].Substring(0, 19), "dd/MM/yyyy hh:mm:ss", culture));
                            }
                            else
                            {
                                ConexionDB.AgregarParametroAsociandoValor(":" + cabecera[j], Fila[j]);
                            }
                        }

                        strConsulta = strConsulta + " )";
                        ConexionDB.EjecutarConsultaConParametros(strConsulta);
                        if (ConexionDB.Estado.error_)
                        {
                            string msgError = (cabecera[0] + ", " + Fila[0] + " --- ERROR --- " + ConexionDB.Estado.mensaje);
                            clsLog.Agregar(msgError);
                            ErroresEncontrados++;
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        string msgError = (contenidoFila + " --- ERROR --- " + ex.Message);
                        clsLog.Agregar(msgError);
                        ErroresEncontrados++;
                    }
                }
            }
            srLector.Close();
            try
            {
                hilo.Abort();
            }
            catch (Exception) { }

            if (clsLog.ListaErrores.Count > 0) {
                
                txt.Text = "ERRORES: " + clsLog.ListaErrores.Count + "--VER LOG.txt---------------------------"+Environment.NewLine;
                for (int i = 0; i < clsLog.ListaErrores.Count; i++)
                {
                    string strDel = null;
                    for(int j = 0; j < clsLog.ListaErrores[i].Length; j++)
                    {
                        strDel = strDel + "*";
                    }
                    strDel = strDel + Environment.NewLine;
                    txt.Text = txt.Text+strDel + i + ") " + clsLog.ListaErrores[i] + Environment.NewLine;

                }

                clsLog.GuardarListaErrores();
                if (clsLog.Estado.error_)
                {
                    MessageBox.Show("ERROR AL GUARDAR LOG.txt " + Environment.NewLine +clsLog.Estado.mensaje);
                }

            }
            else
            {
                txt.Text = "REALIZADO SIN ERRORES";
            }

        }

        private void btAceptar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            


        }
    }
}
