﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReportesWS
{
    public partial class cajaTexto : Form
    {
        public cajaTexto(string cadena)
        {
            InitializeComponent();

            textBox1.Text = cadena;

        }

        private void cajaTexto_Load(object sender, EventArgs e)
        {
            MaximizeBox = false;
        }
    }
}
