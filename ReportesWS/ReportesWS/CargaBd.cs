﻿/*
 * Created by SharpDevelop.
 * User: Luck
 * Date: 12/02/2017
 * Time: 11:52
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;


	/// <summary>
	/// Description of CargaBd.
	/// </summary>
	public partial class CargaBd : Form
	{

        public static bool CancelarCarga;
        public bool MuestraErrores;
        public static int MAX;
        public static int Iterador;
        public static string Mensaje;
        public static string MensajeDefecto;
		public CargaBd()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
            CancelarCarga = false;
            MAX = 0;
            Iterador = 0;
            MuestraErrores = false;
            MensajeDefecto = "Iniciando...";
        }
		
		public void Mostrar(){
            CargaBd carga = new CargaBd();
	        carga.ShowDialog();
		}
		
		void CargaBdLoad(object sender, EventArgs e)
		{
        
        }
		void GroupBox1Enter(object sender, EventArgs e)
		{
	
		}

    private void timer1_Tick(object sender, EventArgs e)
    {

        if (MAX == 0)
        {
            lbMensaje.Text = MensajeDefecto;
            return;
        }

        if (Iterador <= MAX)
        {
            if (MuestraErrores) { 
                lbMensaje.Text = Mensaje+Environment.NewLine+ Iterador + " de " + MAX+Environment.NewLine+ "Errores:" + ReportesWS.Importar.ErroresEncontrados;
            }
            else
            {
                lbMensaje.Text = Mensaje+Environment.NewLine+ Iterador + " de " + MAX;
            }
        }

    }

    private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
        CancelarCarga = true;
    }
}

