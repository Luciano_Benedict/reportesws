﻿
using System.Collections.Generic;
using System.Web;
using System.IO;
using System;
public class InOut
{
    private StreamReader srLector;
    private StreamWriter srEscritor;
    private string _ruta;
    private string _strCadena;
    private stcEstado _Estado;
    public struct stcEstado
    {
        public string mensaje;
        public string observacion;
        public bool error_;
    }

    public InOut()
    {
        _ruta = null;
        _strCadena = null;
        initEstado();
    }

    public InOut(string rutaArchivo)
    {
        AsigarRutaArchivo(rutaArchivo);
        initEstado();
    }

    public stcEstado Estado
    {
        get
        {
            return _Estado;
        }
    }

    public string Out
    {
        get
        {
            return _strCadena;
        }
    }

    public void AsigarRutaArchivo(string rutaArchivo)
    {
        _strCadena = null;
        _ruta = rutaArchivo;
    }

    public bool Escribir(string CadenaContenido)
    {
        try
        {
            srEscritor = new StreamWriter(_ruta);
            srEscritor.Write(CadenaContenido);
            srEscritor.Close();
            srEscritor.Dispose();
        }
        catch (Exception generatedExceptionName)
        {
            _Estado.error_ = true;
            _Estado.mensaje = generatedExceptionName.Message;
            _Estado.observacion = "Error al tratar de ESCRIBIR";
            return false;
        }

        return true;
    }



    public bool EscribirLinea(string CadenaContenido)
    {
        try
        {
            srEscritor = new StreamWriter(_ruta);
            srEscritor.WriteLine(CadenaContenido);
            srEscritor.Close();
            srEscritor.Dispose();
            initEstado();
        }
        catch (Exception generatedExceptionName)
        {
            _Estado.error_ = true;
            _Estado.mensaje = generatedExceptionName.Message;
            _Estado.observacion = "Error al tratar de ESCRIBIR Linea";
            return false;
        }

        return true;
    }

    public bool Escribir(List<string> ListaContenido)
    {
        try
        {
            initEstado();
            srEscritor = new StreamWriter(_ruta);
            for(int i = 0; i < ListaContenido.Count; i++)
            {
                srEscritor.WriteLine(ListaContenido[i]);

            }
            srEscritor.Close();
            srEscritor.Dispose();
            
        }
        catch (Exception generatedExceptionName)
        {
            _Estado.error_ = true;
            _Estado.mensaje = generatedExceptionName.Message;
            _Estado.observacion = "Error al tratar de ESCRIBIR LISTA";
            return false;
        }

        return true;
    }


    public string Leer(string RUTA)
    {
        try
        {
            srLector = new StreamReader(RUTA);
            _strCadena = srLector.ReadToEnd();
            srLector.Close();
            srLector.Dispose();
            initEstado();
        }
        catch (Exception generatedExceptionName)
        {
            _Estado.error_ = true;
            _Estado.mensaje = generatedExceptionName.Message;
            _Estado.observacion = "Error al tratar de LEER Linea";
            return null;
        }

        return _strCadena;
    }

    public List<string> ObtenerListaPorLinea(string RUTA)
    {
        List<string> Cadena = new List<string>();
       
        Cadena.Capacity = 999999;

       
        try
        {
            srLector = new StreamReader(RUTA);
            while(true)
            {
                _strCadena = srLector.ReadLine();
                if (string.IsNullOrEmpty(_strCadena))
                {
                    break;
                }

               Cadena.Add(_strCadena);
               initEstado();

            }
        }
        catch (Exception generatedExceptionName)
        {
            _Estado.error_ = true;
            _Estado.mensaje = generatedExceptionName.Message;
            _Estado.observacion = "Error al tratar de LEER Linea";
            return null;
        }
        srLector.Close();
        srLector.Dispose();
        return Cadena;
    }

    public string Leer()
    {
        try
        {
            srLector = new StreamReader(_ruta);
            _strCadena = srLector.ReadToEnd();
            srLector.Close();
            srLector.Dispose();
            initEstado();
        }
        catch (Exception generatedExceptionName)
        {
            _Estado.error_ = true;
            _Estado.mensaje = generatedExceptionName.Message;
            _Estado.observacion = "Error al tratar de LEER";
            return null;
        }

        return _strCadena;
    }

    private void initEstado()
    {
        _Estado.error_ = false;
        _Estado.mensaje = "OK";
        _Estado.observacion = "";
    }
}

