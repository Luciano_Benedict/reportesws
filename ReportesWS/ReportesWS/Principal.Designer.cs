﻿namespace ReportesWS
{
    partial class Principal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.gbProveedor = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lsErrores = new System.Windows.Forms.ListBox();
            this.lbNro = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbDescripcion = new System.Windows.Forms.Label();
            this.lbError = new System.Windows.Forms.Label();
            this.grilla = new System.Windows.Forms.DataGridView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cbLotes = new System.Windows.Forms.ToolStripComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.gbProveedor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grilla)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbProveedor
            // 
            this.gbProveedor.Controls.Add(this.button3);
            this.gbProveedor.Controls.Add(this.button2);
            this.gbProveedor.Controls.Add(this.button1);
            this.gbProveedor.Controls.Add(this.lsErrores);
            this.gbProveedor.Controls.Add(this.lbNro);
            this.gbProveedor.Controls.Add(this.label16);
            this.gbProveedor.Controls.Add(this.lbDescripcion);
            this.gbProveedor.Controls.Add(this.lbError);
            this.gbProveedor.Dock = System.Windows.Forms.DockStyle.Right;
            this.gbProveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbProveedor.Location = new System.Drawing.Point(945, 27);
            this.gbProveedor.Name = "gbProveedor";
            this.gbProveedor.Size = new System.Drawing.Size(417, 547);
            this.gbProveedor.TabIndex = 13;
            this.gbProveedor.TabStop = false;
            this.gbProveedor.Text = "Insumos: Vista Detallada";
            this.gbProveedor.Enter += new System.EventHandler(this.gbProveedor_Enter);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(132, 440);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(260, 21);
            this.button2.TabIndex = 24;
            this.button2.Text = "Estadistica";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(132, 233);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(260, 21);
            this.button1.TabIndex = 23;
            this.button1.Text = "Ver";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lsErrores
            // 
            this.lsErrores.FormattingEnabled = true;
            this.lsErrores.ItemHeight = 16;
            this.lsErrores.Location = new System.Drawing.Point(22, 270);
            this.lsErrores.Name = "lsErrores";
            this.lsErrores.Size = new System.Drawing.Size(370, 164);
            this.lsErrores.TabIndex = 22;
            // 
            // lbNro
            // 
            this.lbNro.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbNro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNro.ForeColor = System.Drawing.Color.Blue;
            this.lbNro.Location = new System.Drawing.Point(132, 44);
            this.lbNro.Name = "lbNro";
            this.lbNro.Size = new System.Drawing.Size(260, 20);
            this.lbNro.TabIndex = 21;
            // 
            // label16
            // 
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(22, 44);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(104, 20);
            this.label16.TabIndex = 20;
            this.label16.Text = "Tramite";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDescripcion
            // 
            this.lbDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDescripcion.ForeColor = System.Drawing.Color.Blue;
            this.lbDescripcion.Location = new System.Drawing.Point(132, 73);
            this.lbDescripcion.Name = "lbDescripcion";
            this.lbDescripcion.Size = new System.Drawing.Size(260, 157);
            this.lbDescripcion.TabIndex = 1;
            // 
            // lbError
            // 
            this.lbError.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbError.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbError.Location = new System.Drawing.Point(22, 73);
            this.lbError.Name = "lbError";
            this.lbError.Size = new System.Drawing.Size(104, 20);
            this.lbError.TabIndex = 0;
            this.lbError.Text = "Descripción";
            this.lbError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grilla
            // 
            this.grilla.AllowUserToAddRows = false;
            this.grilla.AllowUserToDeleteRows = false;
            this.grilla.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.grilla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grilla.Cursor = System.Windows.Forms.Cursors.NoMove2D;
            this.grilla.Dock = System.Windows.Forms.DockStyle.Left;
            this.grilla.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grilla.Location = new System.Drawing.Point(0, 27);
            this.grilla.Name = "grilla";
            this.grilla.Size = new System.Drawing.Size(939, 547);
            this.grilla.TabIndex = 12;
            this.grilla.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grilla_CellContentClick);
            this.grilla.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grInsumos_CellEnter);
            this.grilla.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grilla_RowEnter);
            this.grilla.SelectionChanged += new System.EventHandler(this.grilla_SelectionChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton6,
            this.toolStripButton2,
            this.toolStripLabel1,
            this.cbLotes});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1362, 27);
            this.toolStrip1.TabIndex = 11;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(30, 24);
            this.toolStripLabel1.Text = "Lote";
            // 
            // cbLotes
            // 
            this.cbLotes.BackColor = System.Drawing.SystemColors.Control;
            this.cbLotes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLotes.Name = "cbLotes";
            this.cbLotes.Size = new System.Drawing.Size(121, 27);
            this.cbLotes.SelectedIndexChanged += new System.EventHandler(this.cbLotes_SelectedIndexChanged);
            this.cbLotes.Click += new System.EventHandler(this.cbLotes_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(132, 467);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(260, 21);
            this.button3.TabIndex = 25;
            this.button3.Text = "Estadistica Metodos";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton1.Text = "Importar";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton6.Text = "Recargar";
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::ReportesWS.Properties.Resources.configuration;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(24, 24);
            this.toolStripButton2.Text = "Configurar DB";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 574);
            this.Controls.Add(this.gbProveedor);
            this.Controls.Add(this.grilla);
            this.Controls.Add(this.toolStrip1);
            this.Name = "Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Principal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbProveedor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grilla)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbProveedor;
        private System.Windows.Forms.Label lbNro;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbDescripcion;
        private System.Windows.Forms.Label lbError;
        private System.Windows.Forms.DataGridView grilla;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ListBox lsErrores;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox cbLotes;
        private System.Windows.Forms.Button button3;
    }
}

