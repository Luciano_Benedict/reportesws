﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReportesWS
{
    public partial class EstadisticaMetodos : Form
    {
        List<Principal.MetodoError> MetodoErrores;
        int Lotes;
        int LotesMax;
        int RegXLote;
        public EstadisticaMetodos(List<Principal.MetodoError> _pMetodoErrores, int _pCantLotes, int _pCantLotesMAx, int _pNroRegXLote)
        {
            InitializeComponent();
            Lotes = _pCantLotes;
            LotesMax = _pCantLotesMAx;
            RegXLote = _pNroRegXLote;
            MetodoErrores = _pMetodoErrores;
            Armar();
            ArmarGrafico();

            lbLotes.Text = "Lotes procesados: " + Lotes + "/" + LotesMax;
            lbReg.Text = "Registros por lote: "+RegXLote ;
        }

        void Armar()
        {
            foreach(Principal.MetodoError _MetodoError in MetodoErrores)
            {
                lsMetodos.Items.Add(_MetodoError.Metodo);
            }

        }

        public void ArmarGrafico()
        {
            int i = 0;
            foreach (Principal.MetodoError _MetodoError in MetodoErrores)
            {
                int total = _MetodoError.LCCEM.Sum();

                Grafico.Series.Add(_MetodoError.Metodo + " Cant: " + total );
                Grafico.Series[i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.RangeColumn;
                Grafico.Series[i].Points.Add(total);
                Random R = new Random(i);
                Grafico.Series[i].Color = Color.FromArgb(R.Next(0, 255), R.Next(0, 255), R.Next(0, 255));
                i++;
            }
        }

        private void EstadisticaMetodos_Load(object sender, EventArgs e)
        {
            MaximizeBox = false;
        }

        private void lsMetodos_SelectedIndexChanged(object sender, EventArgs e)
        {
            lsContCodigos.Items.Clear();
            lsCodigos.Items.Clear();

            for(int i=0;i< MetodoErrores[lsMetodos.SelectedIndex].LCEM.Count; i++) {
                lsCodigos.Items.Add(MetodoErrores[lsMetodos.SelectedIndex].LCEM[i].ToString());
                lsContCodigos.Items.Add(MetodoErrores[lsMetodos.SelectedIndex].LCCEM[i].ToString());
            }
        }

        void ExportarForm(string _pDirectorio)
        {
            try
            {
                var frm = Form.ActiveForm;
                using (var bmp = new Bitmap(frm.Width, frm.Height))
                {
                    frm.DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));
                    bmp.Save(_pDirectorio + "\\ESTADISTICA_METODOS.bmp");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al generar imagen de estadistica " + ex.Message);
            }

        }


        void ExportarTxt(string _pDirectorio)
        {
            InOut Escritor = new InOut(_pDirectorio + "\\Estadistica_Metodos.txt");
            List<string> listaEstadistica = new List<string>();
            listaEstadistica.Add("Estadistica de Metodos: " + DateTime.Now);
            listaEstadistica.Add("Lotes procesados: " + Lotes + "/" + LotesMax);
            listaEstadistica.Add("Registros por Lote: " + RegXLote);
            listaEstadistica.Add("Metodos: " + MetodoErrores.Count);
            listaEstadistica.Add("Codigos: " + MetodoErrores.Sum(x => x.LCEM.Count));
            listaEstadistica.Add("Cantidad de errores: " + MetodoErrores.Sum(x => x.LCCEM.Sum()));
            listaEstadistica.Add("-------------------------------");
            listaEstadistica.Add("MetodoError {");
            listaEstadistica.Add("  Codigo ->Cantidad");
            listaEstadistica.Add("  ......");
            listaEstadistica.Add("}");
            listaEstadistica.Add("-------------------------------");

            foreach (Principal.MetodoError _MetodoError in MetodoErrores)
            {
                listaEstadistica.Add(_MetodoError.Metodo + "{");
                for(int i=0;i< _MetodoError.LCEM.Count; i++)
                {
                    listaEstadistica.Add("\t" + _MetodoError.LCEM[i] + "-->" +_MetodoError.LCCEM[i]);
                }

                listaEstadistica.Add("}");
            }

            Escritor.Escribir(listaEstadistica);

            if (Escritor.Estado.error_)
            {
                MessageBox.Show("Error al escribir estadistica en TXT. " + Environment.NewLine + Escritor.Estado.mensaje);
            }

        }

        public void Export()
        {
            string directorio = null;
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                directorio = folderBrowserDialog1.SelectedPath;
                ExportarForm(directorio);
                ExportarTxt(directorio);
                MessageBox.Show("Realizado");
            }

            folderBrowserDialog1.Dispose();
        }


        private void btExportar_Click(object sender, EventArgs e)
        {
            Export();
        }
    }
}
