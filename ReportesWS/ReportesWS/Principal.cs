﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ReportesWS
{
    public partial class Principal : Form
    {
        Thread hiloCarga;
        public static string cadenaConexion;
        public ListaErrores _ListaErrores;
        ConexionOracle Conexion;
        SysConfig _SysConfig;
        DataTable dtReportes;
        int nroErroresProcesando;
        public static int ErroresMaximos;
        public static int LIMITE_REGISTROS_DB;
        int CANT_REG_LOTE;
        int ID_INICIO, ID_FINAL;
        List<int> CodigoErrores;
        List<int> ContCodigoErrores;
        public List<Lote> Lotes;
        public List<MetodoError> MetodoErrores;
        public struct Lote
        {
            public int Nro, ID_INICIO, ID_FINAL;
        }
        public class MetodoError
        {
            public string Metodo { get; set; }
            public List<int> LCEM { get; set; } // LISTA CODIGO ERRORES METODO
            public List<int> LCCEM { get; set; } // LISTA CONTADOR CODIGO ERRORES METODO

            public MetodoError()
            {
                LCEM = new List<int>();
                LCCEM = new List<int>();
            }

        }

        public Principal()
        {
            InitializeComponent();
            _SysConfig = new SysConfig();

            cadenaConexion = _SysConfig.ObtenerCadenaConexion();


            Conexion = new ConexionOracle(cadenaConexion);

            if (string.IsNullOrEmpty(cadenaConexion) || !Conexion.VerificarConexion() )
            {
                
                ConfigDB _ConfigDB = new ConfigDB();

                do {

                    MessageBox.Show("No se ha configurado la conexion o esta es erronea");

                    if (!(_ConfigDB.ShowDialog() == DialogResult.OK))
                    {
                        Environment.Exit(1);
                    }

                    cadenaConexion = _ConfigDB.ObtenerCadenaConexion();
                    Conexion.PrepararConexion(cadenaConexion);

                } while (!Conexion.VerificarConexion());

                _ConfigDB.Dispose();
            }


            MetodoErrores = new List<MetodoError>();
            ErroresMaximos = 10;
            ArmarTabla2();
           
            LIMITE_REGISTROS_DB = Importar.ObtenerCantidadRegistrosDB();
            
            if (LIMITE_REGISTROS_DB > 0)
            {
                MessageBox.Show("Hay " + LIMITE_REGISTROS_DB + " registros actualmente en DB. Especifique la cantidad de registros por lote", "confirmar");
                                
                ReportesWS.txtCantidadMax text = new ReportesWS.txtCantidadMax(LIMITE_REGISTROS_DB);
                if (text.ShowDialog() == DialogResult.OK)
                {
                    CANT_REG_LOTE= text.Valor;
                }
                
            }
            else { 
                CANT_REG_LOTE= 10;
                MessageBox.Show("Los registros por lote seran de " + CANT_REG_LOTE);
            }

            Lotes = new List<Lote>();

            int cantLotes = (int)Math.Ceiling((double)LIMITE_REGISTROS_DB / CANT_REG_LOTE);

            Lotes = new List<Lote>();
            
            
            for (int i = 0; i < cantLotes; i++)
            {
                if (i == 0)
                {
                    ID_INICIO = ObtenerIDMinimoDB();
                    if(ID_INICIO == (-1))
                    {
                        MessageBox.Show("Error al obtener el minimo ID");
                        Application.Exit();
                    }
                    int RES = ID_INICIO % CANT_REG_LOTE;
                    ID_INICIO -= RES;


                }

                Lotes.Add(new Lote { Nro = (i + 1), ID_INICIO = ID_INICIO + (i*CANT_REG_LOTE), ID_FINAL =  ID_INICIO +((i+1)* CANT_REG_LOTE) });
                
            }

            CargarCombo();
            if (cbLotes.Items.Count > 0)
            {
                cbLotes.Text = "1";
            }
            
        }

        void CargarCombo()
        {
            for (int i = 0; i < Lotes.Count; cbLotes.Items.Add(++i)) ;
        }

        void ElaborarEstadistica()
        {
            CodigoErrores = new List<int>();
            ContCodigoErrores = new List<int>();

            for(int i = 0; i < grilla.Rows.Count; i++)
            {
                for(int j = 0; j < (ErroresMaximos); j++)
                {
                    string item = grilla.Rows[i].Cells["ERROR " + (j+1)].Value.ToString();

                    if (!string.IsNullOrEmpty(item)) { 

                        if (!CodigoErrores.Contains(int.Parse(item))){
                            CodigoErrores.Add(int.Parse(item));
                            ContCodigoErrores.Add(1);
                        }
                        else { 
                            ContCodigoErrores[CodigoErrores.IndexOf(int.Parse(item))]++;
                        }
                    }
                }
            }

        }

        void ElaborarEstadistica(ref List<int> _pCodigoErrores, ref List<int> _pContCodigoErrores)
        {
            for (int i = 0; i < grilla.Rows.Count; i++)
            {
                for (int j = 0; j < (ErroresMaximos); j++)
                {
                    string item = grilla.Rows[i].Cells["ERROR " + (j + 1)].Value.ToString();

                    if (!string.IsNullOrEmpty(item))
                    {

                        if (!_pCodigoErrores.Contains(int.Parse(item)))
                        {
                            _pCodigoErrores.Add(int.Parse(item));
                            _pContCodigoErrores.Add(1);
                        }
                        else
                        {
                            _pContCodigoErrores[_pCodigoErrores.IndexOf(int.Parse(item))]++;
                        }
                    }
                }
            }

        }

        void ArmarTabla()
        {
            dtReportes = new DataTable();
            dtReportes.Columns.Add("ID");
            dtReportes.Columns.Add("ID_SOLICITUD_AMDOCS");
            dtReportes.Columns.Add("FECHA_PROCESO");
            dtReportes.Columns.Add("ERROR");
        
        }

        void CargarGrilla()
        {
            Conexion = new ConexionOracle(cadenaConexion);
            if (!Conexion.VerificarConexion())
            {
                MessageBox.Show("No se pudo conectar." + Conexion.Estado.mensaje);
                return;
            }

            CargaDBEstandar cc = new CargaDBEstandar();
            hiloCarga = new Thread(cc.Mostrar);
            hiloCarga.Start();
            Conexion.BuscarResultados("select ID,ID_SOLICITUD_AMDOCS,FECHA_PROCESO,ERROR from TBL_DS_PROCESOS_ANALISIS where rownum<="+LIMITE_REGISTROS_DB);

            if (Conexion.HayResultados())
            {
                for (int i = 0; i < Conexion.ObtenerResultados().Rows.Count; i++)
                {
                    DataRow Fila = dtReportes.NewRow();
                    for (int j = 0; j < Conexion.ObtenerResultados().Columns.Count; j++)
                    {
                        Fila[j] = Conexion.ObtenerResultados().Rows[i][j];
                    }
                    dtReportes.Rows.Add(Fila);;
                }
            }

            try
            {
                hiloCarga.Abort();
            }
            catch (Exception) { }

            grilla.DataSource = dtReportes;
            
        }

        int ObtenerIDMinimoDB()
        {
            ConexionOracle Conexion = new ConexionOracle(cadenaConexion);
            Conexion.BuscarResultados("select MIN(ID) from tbl_ds_procesos_analisis");

            if (Conexion.Estado.error_)
            {
                MessageBox.Show("Error " + Conexion.Estado.mensaje);
                return (-1);
            }

            if (Conexion.HayResultados())
            {
                if (!string.IsNullOrEmpty(Conexion.ObtenerResultados().Rows[0][0].ToString()))
                {
                    return int.Parse(Conexion.ObtenerResultados().Rows[0][0].ToString());
                }
            }

            return (-1);

        }

        bool CargarGrilla2()
        {
            CargaDBEstandar cc = new CargaDBEstandar();
            hiloCarga = new Thread(cc.Mostrar);

            hiloCarga.Start();

            if (ID_INICIO == (-1))
            {
                return false;
            }

            Conexion = new ConexionOracle(cadenaConexion);
            Conexion.AgregarParametroAsociandoValor("IdInicio", Lotes[cbLotes.SelectedIndex].ID_INICIO);
            Conexion.AgregarParametroAsociandoValor("IdFinal", Lotes[cbLotes.SelectedIndex].ID_FINAL);
            Conexion.BuscarResultadosConParametros("select ID_SOLICITUD_AMDOCS, ERROR from TBL_DS_PROCESOS_ANALISIS where ID >= :IdInicio  AND ID < :IdFinal ");

            if (Conexion.Estado.error_)
            {
                MessageBox.Show("Error. "+ Environment.NewLine+ Conexion.Estado.mensaje);
                return false;
            }

            dtReportes.Rows.Clear();

            if (Conexion.HayResultados())
            {
                
                for (int i = 0; i < Conexion.ObtenerResultados().Rows.Count; i++)
                {
                    DataRow Fila = dtReportes.NewRow();
                  
                    Fila["NRO TRAMITE"] = Conexion.ObtenerResultados().Rows[i]["ID_SOLICITUD_AMDOCS"];
                    Fila["ERROR DETALLADO"] = Conexion.ObtenerResultados().Rows[i]["ERROR"];

                    dtReportes.Rows.Add(Fila);
                    
                }
            }
            grilla.DataSource = dtReportes;

            for (int i = 0; i < ErroresMaximos; i++)
            {
                grilla.Columns["ERROR " + (i + 1)].Width = 50;
                grilla.Columns["METODO " + (i + 1)].Width = 200;
            }

            try
            {
                hiloCarga.Abort();
            }
            catch (Exception) { }

            return true;
        }

        bool CargarGrilla2( int _pInd)
        {
            CargaDBEstandar cc = new CargaDBEstandar();
            hiloCarga = new Thread(cc.Mostrar);

            hiloCarga.Start();

            if (ID_INICIO == (-1))
            {
                return false;
            }

            Conexion = new ConexionOracle(cadenaConexion);
            Conexion.AgregarParametroAsociandoValor("IdInicio", Lotes[_pInd].ID_INICIO);
            Conexion.AgregarParametroAsociandoValor("IdFinal", Lotes[_pInd].ID_FINAL);
            Conexion.BuscarResultadosConParametros("select ID_SOLICITUD_AMDOCS, ERROR from TBL_DS_PROCESOS_ANALISIS where ID >= :IdInicio  AND ID < :IdFinal ");

            if (Conexion.Estado.error_)
            {
                MessageBox.Show("Error. " + Environment.NewLine + Conexion.Estado.mensaje);
                return false;
            }

            dtReportes.Rows.Clear();

            if (Conexion.HayResultados())
            {

                for (int i = 0; i < Conexion.ObtenerResultados().Rows.Count; i++)
                {
                    DataRow Fila = dtReportes.NewRow();

                    Fila["NRO TRAMITE"] = Conexion.ObtenerResultados().Rows[i]["ID_SOLICITUD_AMDOCS"];
                    Fila["ERROR DETALLADO"] = Conexion.ObtenerResultados().Rows[i]["ERROR"];

                    dtReportes.Rows.Add(Fila);

                }
            }
            grilla.DataSource = dtReportes;

            for (int i = 0; i < ErroresMaximos; i++)
            {
                grilla.Columns["ERROR " + (i + 1)].Width = 50;
                grilla.Columns["METODO " + (i + 1)].Width = 200;
            }

            try
            {
                hiloCarga.Abort();
            }
            catch (Exception) { }

            return true;
        }

        void ArmarTabla2()
        {
            dtReportes = new DataTable();
            dtReportes.Columns.Add("NRO TRAMITE");

            for(int i = 0; i < ErroresMaximos; i++) { 
                dtReportes.Columns.Add("METODO "+ (i+1));
                dtReportes.Columns.Add("ERROR " + (i+1));                
            }

            dtReportes.Columns.Add("ERROR DETALLADO");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ProcesarErrores();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            OpenFileDialog AbrirTexto = new OpenFileDialog();
            AbrirTexto.Filter = "Archivos de Texto |*.txt";
            AbrirTexto.FileName = "";
            AbrirTexto.Title = "Seleccionar txt";
            AbrirTexto.InitialDirectory = System.IO.Directory.GetCurrentDirectory().ToString();

            if (AbrirTexto.ShowDialog() == DialogResult.OK)
            {
                string direccion = AbrirTexto.FileName;
                Importar Imp = new Importar(direccion);
                Imp.ShowDialog();
                CargarGrilla2();
                ProcesarErrores();
            }
            else
            {
                MessageBox.Show("No selecciono archivo.");
                return;
            }

        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            grilla.DataSource = null;
            if (CargarGrilla2())
            {
                ProcesarErrores();
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            ConfigDB configDB = new ConfigDB();
            
            if(configDB.ShowDialog() == DialogResult.OK)
            {
                cadenaConexion = configDB.ObtenerCadenaConexion();
                CargarGrilla2();
            }

            configDB.Dispose();
            
        }


        void ProcesarMetodoErrores(int _pindx)
        {

        //LCEM // LISTA CODIGO ERRORES METODO
        //LCCEM// LISTA CONTADOR CODIGO ERRORES METODO

            string strError = grilla.Rows[_pindx].Cells["ERROR DETALLADO"].Value.ToString();
            string strErrorAux = strError.Replace(" ", "");

            string[] strEArray = strErrorAux.Split('-');

            for (int i = 0; i < strEArray.Length; i++) {

                int codError=0;

                if (int.TryParse(strEArray[i],out codError))
                {
                    string strMetodo = strEArray[i - 1];
                    if (string.IsNullOrEmpty(strMetodo))
                    {
                        try { 
                            strMetodo = strEArray[i - 2];
                        }
                        catch (Exception) { strMetodo = "?"; }
                    }
                    MetodoError _MetodoError = MetodoErrores.Find(_m => (_m.Metodo == strMetodo));

                    if (!(_MetodoError == null))
                    {
                        if (_MetodoError.LCEM.Exists(_ce => (_ce == codError)))
                        {
                            MetodoErrores[MetodoErrores.IndexOf(_MetodoError)].LCCEM[_MetodoError.LCEM.IndexOf(codError)]++;
                        }
                        else
                        {
                            MetodoErrores[MetodoErrores.IndexOf(_MetodoError)].LCEM.Add(codError);
                            MetodoErrores[MetodoErrores.IndexOf(_MetodoError)].LCCEM.Add(1);
                        }
                    }
                    else
                    {
                        _MetodoError = new MetodoError();
                        _MetodoError.Metodo = strMetodo;
                        _MetodoError.LCEM.Add(codError);
                        _MetodoError.LCCEM.Add(1);
                        MetodoErrores.Add(_MetodoError);
                    }
                }

            }

        }

        int ProcesarErrores()
        {
            nroErroresProcesando = 0;

            for (int x = 0; x < dtReportes.Rows.Count; x++)
            {

                List<int> codigosErrores = new List<int>();
                List<string> metodosErrores = new List<string>();

                ProcesarMetodoErrores(x);

                string strError = grilla.Rows[x].Cells["ERROR DETALLADO"].Value.ToString();
                string strErrorAux = strError.Replace(" ", "");

                for (int i = 0; (i+4) < strErrorAux.Length; i++)
                {

                    if(strErrorAux.Substring(i,1) == "-" && strErrorAux.Substring(i+4, 1) == "-" )
                    {
                        try
                        {
                            int codError = int.Parse(strErrorAux.Substring(i + 1, 3));
                            codigosErrores.Add(codError);
                        }
                        catch (Exception)
                        {
                            continue;
                        }

                    }

                }

                if (codigosErrores.Count > 0)
                {
                    try { 
                        for(int i = 0; i < codigosErrores.Count; i++) { 

                            int xx = strError.IndexOf("- " + codigosErrores[i] + " -");

                            int xx2 = strError.Substring(0, xx - 1).LastIndexOf("-");

                            string sErr = strError.Substring(xx2 + 2, (xx - (xx2 + 3)));

                            metodosErrores.Add(sErr);

                            grilla.Rows[x].Cells[1 + (i*2)].Value = metodosErrores[i];
                    
                            grilla.Rows[x].Cells[1 +(i*2)+1].Value = codigosErrores[i].ToString();

                        }
                    }
                    catch (Exception ex)
                    {
                        nroErroresProcesando++;
                    }
                }
            }
            return nroErroresProcesando;
        }

        private void grInsumos_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void grilla_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
      
        }

        private void grilla_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(lbDescripcion.Text)) { 
                cajaTexto ct = new cajaTexto(lbDescripcion.Text);
                ct.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int cantLotes = 0;
            
            MessageBox.Show("Ingrese la cantidad de Lotes a realizar la estadistica", "confirmar");

            ReportesWS.txtCantidadMax text = new ReportesWS.txtCantidadMax(Lotes.Count);
            if (text.ShowDialog() == DialogResult.OK)
            {
                cantLotes = text.Valor;
            }
            else
            {
                cantLotes = Lotes.Count;
            }

            List<int> CodigoErrores = new List<int>();
            List<int> ContCodigoErrores = new List<int>();

            int nroErroresProcesando = 0;

            for (int i = 0; i < cantLotes; i++)
            {
                grilla.DataSource = null;
                if (CargarGrilla2(i)) {
                    nroErroresProcesando += ProcesarErrores();
                    ElaborarEstadistica(ref CodigoErrores, ref ContCodigoErrores);
                }
            }

            MessageBox.Show(nroErroresProcesando + " Registros no se pudieron procesar");

            if (CodigoErrores.Count > 0) { 
                Estadistica Est = new Estadistica(CodigoErrores,ContCodigoErrores,cantLotes,Lotes.Count,CANT_REG_LOTE);
                Est.Show();
            }
            
        }

        private void txtCantFilas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                grilla.DataSource = null;
                if (CargarGrilla2()) { 
                    ProcesarErrores();
                }
            }
        }

        private void cbLotes_Click(object sender, EventArgs e)
        {
        
        }

        private void cbLotes_SelectedIndexChanged(object sender, EventArgs e)
        {
            grilla.DataSource = null;
            if (CargarGrilla2())
            {
                ProcesarErrores();
            }
        }

        private void gbProveedor_Enter(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

            int cantLotes = 0;

            MessageBox.Show("Ingrese la cantidad de Lotes a realizar la estadistica", "confirmar");

            ReportesWS.txtCantidadMax text = new ReportesWS.txtCantidadMax(Lotes.Count);
            if (text.ShowDialog() == DialogResult.OK)
            {
                cantLotes = text.Valor;
            }
            else
            {
                cantLotes = Lotes.Count;
            }

            int nroErroresProcesando = 0;

            for (int i = 0; i < cantLotes; i++)
            {
                grilla.DataSource = null;
                if (CargarGrilla2(i))
                {
                    nroErroresProcesando += ProcesarErrores();
                }
            }

            MessageBox.Show(nroErroresProcesando + " Registros no se pudieron procesar");

            if (MetodoErrores.Count == 0)
            {
                MessageBox.Show("No contiene errores");
            }
            else
            {
                EstadisticaMetodos _EstadisticaMetodos = new EstadisticaMetodos(MetodoErrores, cantLotes, Lotes.Count, CANT_REG_LOTE);
                _EstadisticaMetodos.ShowDialog();
            }
        }

        private void grilla_SelectionChanged(object sender, EventArgs e)
        {
            lsErrores.Items.Clear();
            try
            {
                for (int i = 0; i < ErroresMaximos; i++)
                {
                    if (!string.IsNullOrEmpty(grilla.CurrentRow.Cells["METODO " + (1 + i)].Value.ToString()))
                    {
                        lsErrores.Items.Add(grilla.CurrentRow.Cells["METODO " + (1 + i)].Value.ToString() + " - " + grilla.CurrentRow.Cells["ERROR " + (1 + i)].Value.ToString());
                    }
                }

                lbDescripcion.Text = grilla.CurrentRow.Cells["ERROR DETALLADO"].Value.ToString();


                lbNro.Text = grilla.CurrentRow.Cells["NRO TRAMITE"].Value.ToString();

            }
            catch (Exception)
            {
                lsErrores.Items.Clear();
            }
        }

        private void txtCantFilas_Click(object sender, EventArgs e)
        {

        }
    }
}
